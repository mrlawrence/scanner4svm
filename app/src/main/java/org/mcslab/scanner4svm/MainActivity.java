package org.mcslab.scanner4svm;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {


    EditText mET_which_zone ;
    EditText mET_which_uuid ;
    EditText mET_which_major ;
    EditText mET_which_minor ;
    EditText mET_detection_period ;
    Button mBT_start ;
    Button mBT_clear ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mET_which_zone = (EditText)findViewById(R.id.et_which_zone);
        mET_which_uuid = (EditText)findViewById(R.id.et_which_uuid);
        mET_which_major = (EditText)findViewById(R.id.et_which_major);
        mET_which_minor = (EditText)findViewById(R.id.et_which_minor);
        mET_detection_period = (EditText) findViewById(R.id.et_detection_period);
        mBT_start = (Button)findViewById(R.id.bt_start);
        mBT_clear = (Button)findViewById(R.id.bt_clear);


        mBT_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Utils.isDebug) {
                    Log.i(Utils.TAG, "Zone = " + mET_which_zone.getText());
                    Log.i(Utils.TAG, "UUID = " + mET_which_uuid.getText());
                    Log.i(Utils.TAG, "Major = " + mET_which_major.getText());
                    Log.i(Utils.TAG, "Minor = " + mET_which_minor.getText());
                    Log.i(Utils.TAG, "detection period = " + mET_detection_period.getText());
                }

                if(mET_which_zone.getText().toString().equals("")){
                    Log.i(Utils.TAG, "Zone does not set");
                    createDoseNotSetZoneAlert();
                }else{
                    Log.i(Utils.TAG, "Zone set");

                    Intent intent = new Intent();
                    intent.setClass(MainActivity.this, ScanActivity.class);
                    intent.putExtra(Utils.ZONE, mET_which_zone.getText().toString());
                    intent.putExtra(Utils.UUID, mET_which_uuid.getText().toString());
                    intent.putExtra(Utils.MAJOR, mET_which_major.getText().toString());
                    intent.putExtra(Utils.MINOR, mET_which_minor.getText().toString());
                    intent.putExtra(Utils.DETECTION_PERIOD, mET_detection_period.getText().toString());

                    startActivity(intent);
                    MainActivity.this.finish();

                }

            }
        });

        mBT_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mET_which_zone.setText(getString(R.string.default_zone));
                mET_which_uuid.setText(getString(R.string.default_uuid));
                mET_which_major.setText(getString(R.string.default_major));
                mET_which_minor.setText(getString(R.string.default_minor));

            }
        });

    }

    void createDoseNotSetZoneAlert(){

        new AlertDialog.Builder(MainActivity.this)
                .setTitle(getString(R.string.set_the_zone))
                .setMessage(getString(R.string.set_the_zone))
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
    }
}
