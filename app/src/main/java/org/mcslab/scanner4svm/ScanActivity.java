package org.mcslab.scanner4svm;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

public class ScanActivity extends AppCompatActivity {

    Intent intent ;

    TextView mTV_Zone ;
    TextView mTV_UUID ;
    TextView mTV_Major ;
    TextView mTV_Minor ;
    TextView mTV_Detection_Period ;
    EditText mET_Show_Result ;

    String zone = "" ;
    String UUID = "" ;
    int major = -1 ;
    int minor = -1 ;
    int detection_period = 1000 ;

    private BeaconManager beaconManager;
    private Region region;
    private UUID uuid ;

    SimpleDateFormat nowdate = new java.text.SimpleDateFormat("HH:mm:ss:SSS");
    String sdate = null ;

    File file = null ;
    private final static String filename = "scanner4svm.txt" ;
    FileOutputStream outputStream;

    TelephonyManager tManager ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        getparameter() ;
        createView() ;

        tManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);


        file = new File(Environment.getExternalStorageDirectory() + "/"+filename);



        nowdate.setTimeZone(TimeZone.getTimeZone("GMT+8"));

        mET_Show_Result.append(R.string.detect_result + "\n");

        beaconManager = new BeaconManager(this);

        if(UUID.equals("")){
            region = new Region("ranged region", null, null, null);
        }else if(major == -1){

            region = new Region("ranged region",
                    java.util.UUID.fromString(UUID), null, null);
        }else{
            region = new Region("ranged region",
                    java.util.UUID.fromString(UUID), major, null);
        }

        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, List<Beacon> list) {

                if (!list.isEmpty()) {

                    sdate = nowdate.format(new java.util.Date());

                    outputData(sdate, list);
                    //for(Beacon mBeacon : list){
                    //    Log.i(Utils.TAG,sdate+ " "+ mBeacon.getProximityUUID() + " " + mBeacon.getMajor()+ " " +mBeacon.getMinor()+ " "+mBeacon.getRssi());
                    //}
                }
            }
        });

        beaconManager.setBackgroundScanPeriod(detection_period,0);
        beaconManager.setForegroundScanPeriod(detection_period, 0);

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startRanging(region);
            }
        });


    }


    public void getparameter(){
        intent = getIntent() ;
        zone = intent.getStringExtra(Utils.ZONE) ;
        UUID = intent.getStringExtra(Utils.UUID) ;

        if(intent.getStringExtra(Utils.MAJOR).equals("")){
            major = -1 ;
        }else {
            major = Integer.parseInt(intent.getStringExtra(Utils.MAJOR));
        }

        if(intent.getStringExtra(Utils.MINOR).equals("")){
            minor = -1 ;
        }else {
            minor = Integer.parseInt(intent.getStringExtra(Utils.MINOR));
        }

        detection_period = Integer.parseInt(intent.getStringExtra(Utils.DETECTION_PERIOD));
    }

    public void createView(){
        mTV_Zone = (TextView) findViewById(R.id.tv_Zone);
        mTV_UUID = (TextView) findViewById(R.id.tv_UUID);
        mTV_Major = (TextView) findViewById(R.id.tv_Major);
        mTV_Minor = (TextView) findViewById(R.id.tv_Minor);
        mTV_Detection_Period = (TextView) findViewById(R.id.tv_detection_period);
        mET_Show_Result = (EditText) findViewById(R.id.et_show_result);


        mTV_Zone.setText("Zone : " + zone);
        mTV_UUID.setText("UUID : " + UUID);
        mTV_Major.setText("Major : " + major);
        mTV_Minor.setText("Minor : " + minor);
        mTV_Detection_Period.setText("Detection Period : " + detection_period + " ms");

    }


    public void outputData(String date, List<Beacon> list ) {


        String mJson = getJsonObj(date, list);

        //Log.i(Utils.TAG,file.getAbsolutePath());

        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        mET_Show_Result.append(mJson + "\n");


        try{
            outputStream = new FileOutputStream(file, true);
            outputStream.write(mJson.getBytes());
            outputStream.write("\n".getBytes());
            outputStream.flush();
            outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

    }

    public String getJsonObj(String date, List<Beacon> list){

        JSONObject mResultObj = new JSONObject() ;
        JSONArray mBeaconArray = new JSONArray();
        JSONObject mBeaconObj ;

        try {
            mResultObj.put("date" , date) ;
            mResultObj.put("deviceID", tManager.getDeviceId());
            mResultObj.put("zone", zone);

            for (Beacon mBeacon : list) {
                mBeaconObj = new JSONObject() ;
                mBeaconObj.put("UUID", mBeacon.getProximityUUID());
                mBeaconObj.put("major", mBeacon.getMajor());
                mBeaconObj.put("minor", mBeacon.getMinor());
                mBeaconObj.put("macAddress", mBeacon.getMacAddress());
                mBeaconObj.put("measurePower", mBeacon.getMeasuredPower());
                mBeaconObj.put("Rssi", mBeacon.getRssi());
                mBeaconArray.put(mBeaconObj) ;
            }

            mResultObj.put("beacons", mBeaconArray) ;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i(Utils.TAG, mResultObj.toString());
        return mResultObj.toString();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        beaconManager.stopRanging(region);

        Intent intent = new Intent();
        intent.setClass(ScanActivity.this, MainActivity.class);
        startActivity(intent);
        ScanActivity.this.finish();

    }
}
